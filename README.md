<!-- @format -->

![Logo](https://www.idmerit.com/wp-content/themes/idmerit/images/logo-black-txt.png)

# IDMscan SDK for iOS and Android

[![Android](https://img.shields.io/badge/Android-Java_|_Kotlin-brightgreen)](https://www.abc.com/) [![Android-Platform](https://img.shields.io/badge/platform-Android_Studio-red)](https://www.abc.com/)

[![iOS](https://img.shields.io/badge/iOS-Swift_|_Objective_C-blue)](https://www.abc.com/) [![iOS-Platform](https://img.shields.io/badge/platform-Xcode-red)](https://www.abc.com/)

## Version 1.4.4 Update

The version 1.4.4 of IDMscan for iOS and Android introduces the following enhancements:

- **Performance Optimization**: Fine-tuned the performance to ensure smoother operation.
- **Package Support for Latest OS**: Fully compatible with the latest operating systems on both iOS and Android.
- **Bug Fixes**: Various bugs have been addressed to enhance stability and reliability.
- **New Document Support**: Added support for new documents.

## Integration

The documentation outlines the methods for integrating the application.

### Support

For support, email support@idmerit.com.
