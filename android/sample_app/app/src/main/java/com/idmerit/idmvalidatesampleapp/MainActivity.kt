package com.idmerit.idmvalidatesampleapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.idmerit.idmvalidate.data.model.Environment
import com.idmerit.idmvalidate.data.model.Configuration
import com.idmerit.idmvalidate.data.model.DetailedVerification
import com.idmerit.idmvalidate.data.model.IDMConf
import com.idmerit.idmvalidate.data.util.StatusPublisherImpl
import com.idmerit.idmvalidate.data.util.StatusSubscriber
import com.idmerit.idmvalidate.ui.main.view.IDMvalidateActivity

class MainActivity : AppCompatActivity(), StatusSubscriber {

    val consumerKey = "25caba0764bd4c569d0c34ec086e004c"
    //val consumerKey = "4aeb0f795fbf48d8a365c34cdef856bb"
    val consumerSecret = "d5b5f2a876e1458ba18bba7edcfd5ef6"
    //val consumerSecret = "c38c07d82ed44e109b2bb0a1294ee21f"

    val constIDConf= "idm_conf"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val statusPublisherImpl = StatusPublisherImpl()
        statusPublisherImpl.register(this)

        val initButton: Button = findViewById(R.id.init_button)
        initButton.setOnClickListener {
            clickNext()
        }
        clickNext()
    }

    private fun clickNext() {
        val configuration = Configuration(hideFooter = false, skipInputName = false)
        val idmConf = IDMConf(
            consumerKey = consumerKey,
            consumerSecret = consumerSecret,
            environment = Environment.DEVELOPMENT,
            configuration = configuration
        )
        val nextIntent = Intent(this, IDMvalidateActivity::class.java)
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextIntent.putExtra(constIDConf, idmConf)
        this.startActivity(nextIntent)
    }

    override fun status(detailedVerification: DetailedVerification) {
        detailedVerification.identifier?.let { Log.v("Unique ID = ", it) }
        detailedVerification.status?.let { Log.v("Status = ", it) }
    }

}