/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.idmerit.idmvalidatesampleapp;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.idmerit.idmvalidate";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 39;
  public static final String VERSION_NAME = "1.0.39";
}
