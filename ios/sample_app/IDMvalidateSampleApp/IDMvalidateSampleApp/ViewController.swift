//
//  ViewController.swift
//  IDMvalidateSampleApp
//
//  Created by Dilshan Edirisuriya on 2021-06-18.
//

import UIKit
import IDMvalidate

class ViewController: UIViewController {

    static let consumerKey: String = "ec8d6e30b0e244c58225097e80c17a0d"
    static let consumerSecret: String = "6ab95fb3d2434914bc470b0b4ca18bd5"
    static let notificationName: String = "verification"
    static let keyPair = KeyPair.init(consumerKey: consumerKey, consumerSecret: consumerSecret)
    static let configuration = Configuration.init(logo: UIImage.init(named: "logo")!, primaryColor: UIColor(red: 4.0/255.0, green: 67.0/255.0, blue: 126.0/255.0, alpha: 1.0), hideFooter: false, skipInputName: false)
    static var idmValidateManager = IDMvalidateManager(environment: Environment.DEVELOPMENT, keyPair: keyPair, configuration: configuration, notificationName: notificationName)
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.verificationCompletion(_:)), name: NSNotification.Name(rawValue: ViewController.notificationName), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let viewController = ViewController.idmValidateManager.viewController()
        self.navigationController?.pushViewController(viewController, animated: false)
    }

    @objc func verificationCompletion(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            let status = (dict["status"] as? String)!
            let uniqueID = (dict["uniqueID"] as? String)!
            print("Status : " + status)
            print("Unique ID : " + uniqueID)
            
            let verificationDetails:DetailedVerification = ViewController.idmValidateManager.verificationResults()
            print("Status : " + (verificationDetails.status ?? ""))
            print("Request ID : " + (verificationDetails.requestId ?? ""))
            print("Identifier : " + (verificationDetails.identifier ?? ""))
            print("Name : " + (verificationDetails.name ?? ""))
            print("DOB : " + (verificationDetails.dateOfBirth ?? ""))
            print("Document Type : " + (verificationDetails.documentType ?? ""))
            print("Country : " + (verificationDetails.country ?? ""))
            print("Scan Image : " + (verificationDetails.scanImage ?? ""))
            print("Selfie Image : " + (verificationDetails.selfieImage ?? ""))
            print("Mobile : " + (verificationDetails.mobile ?? ""))
            print("Country Code : " + (verificationDetails.countryCode ?? ""))
            print("Requested Time : " + (verificationDetails.requestedTime ?? ""))
            print("Validated Time : " + (verificationDetails.validatedTime ?? ""))
            print("Face Matches : " + (verificationDetails.faceMatches ?? ""))
            print("Liveness : " + (verificationDetails.liveness ?? ""))
            print("Document Score : " + (verificationDetails.documentScore ?? ""))
            print("Risk Factor : " + (verificationDetails.riskFactor ?? ""))
            print("Name Score : " + (verificationDetails.nameScore ?? ""))
            print("Device ID : " + (verificationDetails.deviceId ?? ""))
            print("Latitude : " + String(describing: verificationDetails.latitude))
            print("Longitude : " + String(describing: verificationDetails.longitude))
            print("User Agent : " + (verificationDetails.userAgent ?? ""))
            print("DOB Score : " + (verificationDetails.dobScore ?? ""))
            print("Forgery Status : " + (verificationDetails.forgeryStatus ?? ""))
            print("Forgery Results : " + (verificationDetails.forgeryResult ?? ""))
            print("Status Info : " + (verificationDetails.statusInfo ?? ""))
            print("Failure Reason : " + (verificationDetails.failureReason ?? ""))
            print("QR Code : " + (verificationDetails.qrCode ?? ""))

            for (key,value) in verificationDetails.barcodeMap ?? [:] {
                print("\(key) : \(value)")
            }
            
            let alert = UIAlertController(title: "Response", message: ("Status = " + status + ", Unique ID = " + uniqueID), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .cancel: break
                    case .default: break
                    case .destructive: break
                    @unknown default: break
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

